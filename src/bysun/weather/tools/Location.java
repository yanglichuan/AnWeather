package bysun.weather.tools;

import java.io.Serializable;

/**
 * 地点信息
 */
public class Location implements Serializable {
    private static final long serialVersionUID = 6200150367469420497L;
    /** 地点编码 */
    private String code;
    /** 地点名称 */
    private String name;
    /** 地点邮编 */
    private String zipCode;
    /** 地点电话区号 */
    private String areaCode;
    /** 地点拼音名 */
    private String pinyinName;

    /**
     * 获取 areaCode (地点电话区号) 的值
     *
     * @return areaCode (地点电话区号) 的值
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 字段 areaCode (地点电话区号) 赋值
     *
     * @param areaCode (地点电话区号)
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * 获取 code (地点编码) 的值
     *
     * @return code (地点编码) 的值
     */
    public String getCode() {
        return code;
    }

    /**
     * 字段 code (地点编码) 赋值
     *
     * @param code (地点编码)
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取 name (地点名称) 的值
     *
     * @return name (地点名称) 的值
     */
    public String getName() {
        return name;
    }

    /**
     * 字段 name (地点名称) 赋值
     *
     * @param name (地点名称)
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取 pinyinName (地点拼音名) 的值
     *
     * @return pinyinName (地点拼音名) 的值
     */
    public String getPinyinName() {
        return pinyinName;
    }

    /**
     * 字段 pinyinName (地点拼音名) 赋值
     *
     * @param pinyinName (地点拼音名)
     */
    public void setPinyinName(String pinyinName) {
        this.pinyinName = pinyinName;
    }

    /**
     * 获取 zipCode (地点邮编) 的值
     *
     * @return zipCode (地点邮编) 的值
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * 字段 zipCode (地点邮编) 赋值
     *
     * @param zipCode (地点邮编)
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
